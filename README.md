#### Tech stack
- React
- Express 
- Node
- MongoDB

## Run Locally

Clone the project (via HTTP)

```bash
  git clone --single-branch -b development https://gitlab.com/assessment742751/evotech-assessment.git
```

Clone the project (via SSH)

```bash
  git clone --single-branch -b development git@gitlab.com:assessment742751/evotech-assessment.git
```

Go to the project directory

```bash
  For front-end:
  cd evotech-assessment/frontend/survey_ui
```

```bash
  For backed-end:
  cd evotech-assessment/backend
```

Install dependencies(for front-end)

```bash
  npm install or yarn
```

Install dependencies(for back-end)

```bash
  npm install
```

Start the app(for front-end) Local

```bash
  1) Create .env and give REACT_APP_NODE_ENV=development
  2) npm run start or yarn start
```

Start the server(for back-end) Local

```bash
  1) Create .env give PORT and MONGO_URL
  2) npm run start
```
