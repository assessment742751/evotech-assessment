import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import './surveyList.css'; // Import the CSS file

const SurveyList = ({ token }) => {
    const [surveys, setSurveys] = useState([]);
    const [loading, setLoading] = useState(true);
    const navigate = useNavigate();

    useEffect(() => {
        const fetchSurveys = async () => {
            try {
                const nodeEnv = process.env.REACT_APP_NODE_ENV || "production";
                const apiBaseUrl = nodeEnv === 'production'
                    ? 'https://evotech-survey-app-assessment.onrender.com'
                    : 'http://localhost:8000';
                const response = await axios.get(`${apiBaseUrl}/api/surveys`, {
                    headers: {
                        Authorization: `${token}`, // Attach the token to the request headers
                    },
                });
                setSurveys(response.data);
                setLoading(false);
            } catch (error) {
                console.error('Error fetching surveys:', error);
                navigate('/admin-login');

            }
        };

        fetchSurveys();
    }, [token]);

    return (
        <div className="survey-list-container">
            <h2 className="survey-list-heading">Survey List</h2>
            {loading ? (
                <p className="loading-message">Loading surveys...</p>
            ) : (
                <table className="survey-table">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone Number</th>
                            <th>Gender</th>
                            <th>Nationality</th>
                            <th>Address</th>
                            <th>Message</th>
                        </tr>
                    </thead>
                    <tbody>
                        {surveys.map((survey) => (
                            <tr key={survey._id}>
                                <td>{survey.name}</td>
                                <td>{survey.email}</td>
                                <td>{survey.phoneNumber}</td>
                                <td>{survey.gender}</td>
                                <td>{survey.nationality}</td>
                                <td>{survey.address}</td>
                                <td>{survey.message}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            )}
        </div>
    );
};

export default SurveyList;
