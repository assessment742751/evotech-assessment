import React, { useState } from 'react';
import axios from 'axios';
import './surveyForm.css'; // Import the CSS file

const SurveyForm = () => {
    const [formData, setFormData] = useState({
        name: '',
        gender: '',
        nationality: '',
        email: '',
        phoneNumber: '',
        address: '',
        message: '',
    });
    const [formErrors, setFormErrors] = useState({
        name: '',
        gender: '',
        nationality: '',
        email: '',
        phoneNumber: '',
        address: '',
        message: '',
    });

    const handleSubmit = async (e) => {
        e.preventDefault();

        try {
            const nodeEnv = process.env.REACT_APP_NODE_ENV || "production";
            const apiBaseUrl = nodeEnv === 'production'
                ? 'https://evotech-survey-app-assessment.onrender.com'
                : 'http://localhost:8000';
            // Submit the form data
            const response = await axios.post(`${apiBaseUrl}/api/surveys/`, formData);
            // If successful submission
            // Clear the form fields
            setFormData({
                name: '',
                gender: '',
                nationality: '',
                email: '',
                phoneNumber: '',
                address: '',
                message: '',
            });
            // Clear form errors
            setFormErrors({
                name: '',
                gender: '',
                nationality: '',
                email: '',
                phoneNumber: '',
                address: '',
                message: '',
            });
            window.alert(`${response.data.message}`);
        } catch (error) {
            console.log(error.response.data.errors)
            if (error.response.data.errors) {
                // If there are errors in the response, update the formErrors state
                const newFormErrors = {};
                error.response.data.errors.forEach((error) => {
                    newFormErrors[error.path] = error.msg;
                });
                setFormErrors(newFormErrors);
                return;
            }
            console.error('Error submitting survey:', error.message);
        }
    };

    return (
        <div className="survey-form-container">
            <h2 className="survey-form-heading">EvoTech Survey Form</h2>
            <form onSubmit={handleSubmit} className="survey-form">
                <label className={`survey-label ${formErrors.name && 'error'}`}>
                    Name:
                    <input
                        type="text"
                        value={formData.name}
                        onChange={(e) => setFormData({ ...formData, name: e.target.value })}
                        className={`survey-input ${formErrors.name && 'error'}`}
                    />
                    {formErrors.name && <span className="error-message">{formErrors.name}</span>}
                </label>

                <label className={`survey-label ${formErrors.gender && 'error'}`}>
                    Gender:
                    <select
                        value={formData.gender}
                        onChange={(e) => setFormData({ ...formData, gender: e.target.value })}
                        className={`survey-input ${formErrors.gender && 'error'}`}
                    >
                        <option value="">Select Gender</option>
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                    </select>
                    {formErrors.gender && <span className="error-message">{formErrors.gender}</span>}
                </label>

                <label className={`survey-label ${formErrors.nationality && 'error'}`}>
                    Nationality:
                    <select
                        value={formData.nationality}
                        onChange={(e) => setFormData({ ...formData, nationality: e.target.value })}
                        className={`survey-input ${formErrors.nationality && 'error'}`}
                    >
                        <option value="">Select Nationality</option>
                        <option value="usa">United States</option>
                        <option value="canada">Canada</option>
                        <option value="India">India</option>
                        <option value="Other">Other</option>
                    </select>
                    {formErrors.nationality && <span className="error-message">{formErrors.nationality}</span>}
                </label>

                <label className={`survey-label ${formErrors.email && 'error'}`}>
                    Email:
                    <input
                        type="email"
                        value={formData.email}
                        onChange={(e) => setFormData({ ...formData, email: e.target.value })}
                        className={`survey-input ${formErrors.email && 'error'}`}
                    />
                    {formErrors.email && <span className="error-message">{formErrors.email}</span>}
                </label>

                <label className={`survey-label ${formErrors.phoneNumber && 'error'}`}>
                    Phone Number:
                    <input
                        type="tel"
                        value={formData.phoneNumber}
                        onChange={(e) => setFormData({ ...formData, phoneNumber: e.target.value })}
                        className={`survey-input ${formErrors.phoneNumber && 'error'}`}
                    />
                    {formErrors.phoneNumber && <span className="error-message">{formErrors.phoneNumber}</span>}
                </label>

                <label className={`survey-label ${formErrors.address && 'error'}`}>
                    Address:
                    <textarea
                        value={formData.address}
                        onChange={(e) => setFormData({ ...formData, address: e.target.value })}
                        className={`survey-input ${formErrors.address && 'error'}`}
                    />
                    {formErrors.address && <span className="error-message">{formErrors.address}</span>}
                </label>

                <label className={`survey-label ${formErrors.message && 'error'}`}>
                    Message:
                    <textarea
                        value={formData.message}
                        onChange={(e) => setFormData({ ...formData, message: e.target.value })}
                        className={`survey-input ${formErrors.message && 'error'}`}
                    />
                    {formErrors.message && <span className="error-message">{formErrors.message}</span>}
                </label>

                <button type="submit" className="survey-button">
                    Submit Survey
                </button>
            </form>
        </div>
    );
};

export default SurveyForm;
