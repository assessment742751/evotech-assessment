import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import './adminLogin.css'; // Import the CSS file

const AdminLogin = ({ onLogin }) => {
    const [credentials, setCredentials] = useState({ username: '', password: '' });
    const [loginStatus, setLoginStatus] = useState(null);
    const navigate = useNavigate();

    const handleLogin = async (e) => {
        e.preventDefault();

        try {
            const nodeEnv = process.env.REACT_APP_NODE_ENV || "production";
            const apiBaseUrl = nodeEnv === 'production'
                ? 'https://evotech-survey-app-assessment.onrender.com'
                : 'http://localhost:8000';
            const response = await axios.post(`${apiBaseUrl}/api/admin/login`, credentials);
            setLoginStatus(response.data.message);
            onLogin(response.data.token);

            navigate('/surveys');
        } catch (error) {
            setLoginStatus('Invalid username or password. Please try again.');
        }
    };

    return (
        <div className="admin-login-container">
            <h2 className="admin-login-heading">Admin Login</h2>
            {loginStatus && <p className="login-status">{loginStatus}</p>}
            <form onSubmit={handleLogin} className="admin-login-form">
                <label className='admin-label'>
                    Username/Email:
                    <input
                        type="text"
                        value={credentials.username}
                        onChange={(e) => setCredentials({ ...credentials, username: e.target.value })}
                        className="admin-login-input"
                    />
                </label>
                <label className='admin-label'>
                    Password:
                    <input
                        type="password"
                        value={credentials.password}
                        onChange={(e) => setCredentials({ ...credentials, password: e.target.value })}
                        className="admin-login-input"
                    />
                </label>
                <button type="submit" className="admin-login-button">
                    Login
                </button>
            </form>
        </div>
    );
};

export default AdminLogin;
