import React, { useState } from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import SurveyForm from './components/SurveyForm';
import SurveyList from './components/SurveyList';
import AdminLogin from './components/AdminLogin';
import './App.css';

function App() {
  const [token, setToken] = useState(null);
  const currentPath = window.location.pathname;

  const handleLogin = (newToken) => {
    setToken(newToken);
  };

  return (
    <Router>
      <nav>
        <ul>
          {(currentPath !== '/') && (
            <li>
              <a href="/">Survey Form</a>
            </li>
          )}
          {(currentPath !== '/surveys' && currentPath !== '/admin-login') && (
            <li>
              <a href="/admin-login">Show all submissions</a>
            </li>
          )}
        </ul>
      </nav>

      <Routes>
        <Route
          path="/"
          element={
            <SurveyForm />
          }
        />
        <Route
          path="/surveys"
          element={
            <SurveyList token={token} />
          }
        />
        <Route
          path="/admin-login"
          element={
            <AdminLogin onLogin={handleLogin} />
          }
        />
      </Routes>
    </Router>
  );
}

export default App;
