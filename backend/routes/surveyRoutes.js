const express = require('express');
const router = express.Router();
const Survey = require('../models/surveyModel');
const checkUserRole = require('../helper/middleware')
const validateSurvey = require('../helper/validation');
const { validationResult } = require('express-validator');

// POST: Create a new survey
router.post('/', validateSurvey, async (req, res) => {
    try {
        // Validate the request body using express-validator
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }

        const { name, gender, nationality, email, phoneNumber, address, message } = req.body;

        // Create a new survey instance
        const survey = new Survey({
            name,
            gender,
            nationality,
            email,
            phoneNumber,
            address,
            message,
        });

        // Save the survey to the database
        await survey.save();

        res.status(201).json({ message: 'Survey submitted successfully' });
    } catch (error) {
        console.error('Error submitting survey:', error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
});


// Get all surveys
router.get('/', checkUserRole(['admin']), async (req, res) => {
    try {
        const surveys = await Survey.find();
        res.status(200).json(surveys);
    } catch (error) {
        res.status(500).json({ error: 'Internal Server Error' });
    }
});

module.exports = router;
