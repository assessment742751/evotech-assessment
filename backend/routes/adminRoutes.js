const express = require('express');
const Admin = require('../models/adminModel');
const router = express.Router();
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

// Login API endpoint with JWT
router.post('/login', async (req, res) => {
    try {
        const { username, password } = req.body;
        // Check if the admin user exists
        const admin = await Admin.findOne({ username });
        if (!admin || !(await bcrypt.compare(password, admin.password))) {
            return res.status(401).json({ error: 'Invalid credentials' });
        }
        // Create a JWT token
        const token = jwt.sign({ username: admin.username, role: admin.role }, 'secret_key');
        res.status(200).json({ message: 'Login successful!', token });
    } catch (error) {
        console.error('Error during login:', error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
});
// Create admin API endpoint
// API endpoint to create an admin user
router.post('/create', async (req, res) => {
    try {
        const { username, password, role } = req.body;
        // Check if the admin already exists
        const existingAdmin = await Admin.findOne({ username });
        if (existingAdmin) {
            return res.status(400).json({ error: 'Admin with this username already exists.' });
        }
        // Hash the password
        const hashedPassword = await bcrypt.hash(password, 10);
        // Create a new admin user
        const newAdmin = new Admin({ username, password: hashedPassword, role });
        await newAdmin.save();
        res.status(201).json({ message: 'Admin created successfully!' });
    } catch (error) {
        console.error('Error creating admin:', error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
});
module.exports = router;
