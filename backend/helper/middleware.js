const jwt = require('jsonwebtoken');

const checkUserRole = (allowedRoles) => {
    return (req, res, next) => {
        // Extract the token from the Authorization header
        const token = req.header('Authorization');

        if (!token) {
            return res.status(401).json({ error: 'Unauthorized. Token not provided.' });
        }

        try {
            // Verify and decode the token
            const decoded = jwt.verify(token, 'secret_key');

            // Check if the decoded user's role is allowed
            const { role } = decoded;

            if (allowedRoles.includes(role)) {
                // Allow user with specified roles
                next();
            } else {
                res.status(403).json({ error: 'Permission denied. Insufficient privileges.' });
            }
        } catch (error) {
            console.error('JWT verification error:', error);
            return res.status(401).json({ error: 'Unauthorized. Invalid token.' });
        }
    };
};

module.exports = checkUserRole;
