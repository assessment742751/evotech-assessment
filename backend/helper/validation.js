const { body, validationResult } = require('express-validator');

const validateSurvey = [
    body('name').notEmpty().withMessage('Name is required'),
    body('gender').notEmpty().withMessage('Gender is required'),
    body('nationality').notEmpty().withMessage('Nationality is required'),
    body('email').isEmail().withMessage('Valid email is required'),
    body('phoneNumber').notEmpty().withMessage('Phone number is required'),
    body('address').notEmpty().withMessage('Address is required'),
    body('message').notEmpty().withMessage('Message is required'),
];

module.exports = validateSurvey;
