const mongoose = require('mongoose');

const adminSchema = new mongoose.Schema({
    username: String,
    password: String,
    role: {
        type: String,
        enum: ['admin', 'user'],
        default: 'admin',
    }
});

const Admin = mongoose.model('Admin', adminSchema);

module.exports = Admin;
